// ТЕОРІЯ:

/* 
1. Описати своїми словами навіщо потрібні функції у програмуванні.

- для можливості застосування одного шматка коду кілька разів;
- для можливості обробки одним шматком коду різних даних (аргументів);
- для кращої читабельності (самодокументованості) коду.
*/



/*
2. Описати своїми словами, навіщо у функцію передавати аргумент.

Для універсалізації — щоб ті самі дії (описані у тілі функції) можна було виконувати над різними даними.
*/



/*
3. Що таке оператор return та як він працює всередині функції?

Дозволяє видати щось у відповідь на виклик функції та завершує обробку кода всередині функції (бо код після оператора return не виконується).
*/



// ПРАКТИКА:


// Declare the variables:
let x, y, op, result;

// Get two numbers from user:
do {
	x = prompt(`1. Give me a number\nOR CANCEL TO QUIT`, x == null ? "" : x);
	y = prompt(`2. Give me another number\nOR CANCEL TO QUIT`, y == null ? "" : y);
} while (
	(isNaN(x) || isNaN(y) 
	|| x === undefined || y === undefined
	|| x === "" || y === "")
	&& (x !== null || y !== null) // because we allow the user to quit the cycle by hitting CANCEL.
);

// Get the math operator:
do {
	op = prompt(`3. Give me one math operator (+ or - or * or /)\nOR CANCEL TO QUIT`, op == null ? "" : op);
} while (!isValidOperator(op));

// Call the main function:
result = doMath(+x,+y,op); // without the unary plus, the addition fails ("1"+"2"="12" etc).
console.log(result);

// EXTRA EFFORT:
if(isValidOperator(op) && op != null){
	document.getElementById("output").innerHTML = `${x} ${op} ${y} = <strong>${result}</strong>`;
}



// Main function:

function doMath(a, b, mathOperation) {
	switch (mathOperation) {
		case '+':
			return a + b;
		case '-':
			return a - b;
		case '*':
			return a * b;
		case '/':
			return a / b;
		default:
			return "User canceled or unknown error.";
	}
}

// Helper function:
function isValidOperator(mathOperator){
	switch (mathOperator) {
		case '+':
		case '-':
		case '*':
		case '/':
		case null: // because we allow the user to quit the cycle by hitting CANCEL.
			return true;
		default:
			return false;
	}
}